#include <SPI.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme;

const int button_pin = 2;
const int buzzer_pin = 9;
const int blue_led_pin = 10;
const int green_led_pin = 11;
const int red_led_pin = 12;

int button_state = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  bool status;

  // default settings
  status = bme.begin(0x76);
  if (!status) {
      Serial.println("Could not find a valid BME280 sensor, check wiring!");
      while (1);
  }

  // setup button pin
  pinMode(button_pin, INPUT);

  // setup buzzer pin
  pinMode(buzzer_pin, OUTPUT);

  // setup led pin
  pinMode(blue_led_pin, OUTPUT);
  pinMode(green_led_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  main_proc();
  delay(500);
}

void main_proc() {
  float temp = bme.readTemperature();
  
  // show temperature by Celsius or Fahrenheit
  if (get_button_state() == "FALLING") {
    Serial.print(temp);
    Serial.print("C");
    Serial.println();
  } else {
    temp = temp * 1.8 + 32.0; // get Fahrenheit from celsius
    Serial.print(temp);
    Serial.print("F");
    Serial.println();
  }

  // buzzer and led on
  if (temp < 10.0) {
    buzz(500);  
  } else if (10.0 < temp < 37.0) {
    show_led(blue_led_pin);  
    buzz(500);  
  } else if (37.1 < temp < 40.0 ) {
    show_led(green_led_pin);  
  } else if (40.1 < temp < 50.0) {
    show_led(red_led_pin); 
    buzz(1000); 
  }
}

void show_led(int led_pin) {
  digitalWrite(led_pin, HIGH);
  delay(1000);
  digitalWrite(led_pin, LOW);
  delay(1000);
}

void buzz(int frequency) {
  tone(buzzer_pin, frequency);  // Send 1KHz sound signal...
  delay(1000);                  // ...for 1 sec
  noTone(buzzer_pin);           // Stop sound...
  delay(1000); 
}

String get_button_state() {
  button_state = digitalRead(button_pin);
  if (button_state == 1) return "FALLING";
  else return "RISING";
}
